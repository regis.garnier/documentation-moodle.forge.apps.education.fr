// @ts-check
// Note: type annotations allow type checking and IDEs autocompletion

const lightCodeTheme = require('prism-react-renderer/themes/github');
const darkCodeTheme = require('prism-react-renderer/themes/dracula');

/** @type {import('@docusaurus/types').Config} */
const config = {
  title: 'Fiches réflexes Moodle4',
  tagline: ' accompagner dans la prise en main de la plateforme Moodle',
  favicon: 'img/favicon.ico',

  // Set the production url of your site here
  url: 'https://documentation-moodle.forge.apps.education.fr/',
  // Set the /<baseUrl>/ pathname under which your site is served
  // For GitHub pages deployment, it is often '/<projectName>/'
  baseUrl: '/',

  onBrokenLinks: 'warn',
  onBrokenMarkdownLinks: 'warn',

  // Even if you don't use internalization, you can use this field to set useful
  // metadata like html lang. For example, if your site is Chinese, you may want
  // to replace "en" with "zh-Hans".
  i18n: {
    defaultLocale: 'fr',
    locales: ['fr'],
  },

  presets: [
    [
      'classic',
      /** @type {import('@docusaurus/preset-classic').Options} */
      ({
        docs: {
          sidebarPath: require.resolve('./sidebars.js'),
        },
        theme: {
          customCss: require.resolve('./src/css/custom.css'),
        },
      }),
    ],
  ],

  themeConfig:
    /** @type {import('@docusaurus/preset-classic').ThemeConfig} */
    ({
      // Replace with your project's social card
      image: 'img/docusaurus-social-card.jpg',
      navbar: {
        title: '🏠',
        logo: {
          alt: 'accompagner dans la prise en main de la plateforme Moodle',
          src: 'img/logo-iam.png',
        },
        items: [
          {
            type: 'docSidebar',
            sidebarId: 'tutorialSidebar',
            position: 'left',
            label: 'tutoriels',
          },
          {
            type: 'docSidebar',
            sidebarId: 'demonstrationSidebar',
            position: 'left',
            label: 'Démonstration',
          },
          {
            type: 'docSidebar',
            sidebarId: 'scenarioSidebar',
            position: 'left',
            label: 'Scénarios',
          },
          {
            href: 'https://forge.apps.education.fr/documentation-moodle/documentation-moodle.forge.apps.education.fr',
            label: 'Forge',
            position: 'right',
          },
        ],
      },
      algolia: {
        // The application ID provided by Algolia
        appId: 'LPVZ9PP4MH',
  
        // Public API key: it is safe to commit it
        apiKey: '669a0eeca6d6c269d4cdb70ef4f0475a',
  
        indexName: 'umentation-moodle-forges-education',
  
        // Optional: see doc section below
        contextualSearch: true,
  
        // Optional: Specify domains where the navigation should occur through window.location instead on history.push. Useful when our Algolia config crawls multiple documentation sites and we want to navigate with window.location.href to them.
        externalUrlRegex: 'external\\.com|domain\\.com',
  
        // Optional: Replace parts of the item URLs from Algolia. Useful when using the same search index for multiple deployments using a different baseUrl. You can use regexp or string in the `from` param. For example: localhost:3000 vs myCompany.com/docs
        // replaceSearchResultPathname: {
        //  from: '/docs/', // or as RegExp: /\/docs\//
        //  to: '/',
        //},
  
        // Optional: Algolia search parameters
        searchParameters: {},
  
        // Optional: path for search page that enabled by default (`false` to disable it)
        searchPagePath: 'search',
  
        //... other Algolia params
      },
      footer: {
        style: 'dark',
        links: [
            {
            title: 'Communauté',
            items: [
              {
                label: 'IAM',
                href: 'https://iam.unistra.fr',
              },
            ],
          },
           {
            title: 'docusaurus',
            items: [
              {
                label: 'présentation',
                href: 'https://docusaurus.io/',
              },
            ],
          },
          
        ],
        copyright: `Copyright © ${new Date().getFullYear()} Communauté IAM, fait avec Docusaurus.`,
      },
      prism: {
        theme: lightCodeTheme,
        darkTheme: darkCodeTheme,
      },
    }),
};

module.exports = config;
