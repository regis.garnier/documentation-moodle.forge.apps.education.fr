---
sidebar_position: 6
---

# Le suivi d'achèvement

**Dans Moodle, l'enseignant dispose de 2 paramètres pour individualiser les parcours d'apprentissage. **

:::info
 * **Les Restrictions d'accès** : ce paramètre permet de déterminer les conditions qui permettront d'accéder à une activité ou à une ressource
 * **Le Suivi d'achèvement** : ce paramètre permet de déterminer à quelle condition une activité sera considérée comme terminée. Cela permettra d'accéder à la suite du parcours, de recevoir un badge…
**Ces 2 paramètres peuvent être combinés. Nous allons déjà découvrir le suivi d'achèvement.**
:::

## Le suivi d'achèvement

**Le suivi d'achèvement permet d'indiquer qu'une activité est achevée ou non.** Une case apparaît à côté de chaque activité, qui se coche lorsque l'étudiant a terminé l'activité selon les critères d'achèvement. 

:::danger
**Par défaut, ce paramètre n'est pas activé dans votre cours Moodle.**  
Pour l'activer : 

**1.**	Accédez aux **paramètres** de votre cours : <img src="../../../img/suivi/achevement01.jpg" className='printscreen' style={{maxWidth:'300px'}} />

<img src="../../../img/suivi/achevement02.jpg" className='printscreen' style={{float:'right', maxWidth:'200px'}} />

**2.**	Dans les paramètres, à la rubrique **"suivi d'achèvement"**, il faut **"activer le suivi d'achèvement des activités"**.

**NB :**  Un nouveau paramètre permet de choisir si mes conditoons seront affichées sur la page de cours (en plus de l'affichage dans le page de l'activité). 
:::

➡️ Ouvrez les paramètres de la ressource ou de l'activité

### Conditions d'achèvement pour une Ressource (ex. ressource Fichier ou URL)

Il est possible de choisir l'une de ces 2 conditions :
 * <img src="../../../img/suivi/achevement14.jpg" style={{maxWidth:'20px'}} /> Choix 1 : "Les participants peuvent marquer manuellement cette activité comme terminée" : ce sont les élèves qui devront cocher la case à droite de la ressource pour certifier qu'ils l'ont consultée.

<img src="../../../img/suivi/achevement03.jpg" className='printscreen' style={{float:'right',maxWidth:'40%'}} />

 * <img src="../../../img/suivi/achevement15.jpg" style={{maxWidth:'20px'}} /> Choix 2 : "Afficher l'activité comme terminée dès que les conditions sont remplies" : Dans le cas d'une ressource, la seule condition possible est d'avoir affiché cette activité pour la terminer. 
*(ex. les élèves doivent ouvrir un fichier PDF pour que cela soit considéré comme terminé)*


*Affichage sur la page de l'élève :* 

<img src="../../../img/suivi/achevement04.jpg" className='printscreen' style={{float:'left',maxWidth:'80%'}} />

<div style={{clear:'both'}}>&nbsp;</div>


### Conditions d'achèvement pour une Activité (ex. un TEST)

**Les conditions d'achèvement dépendent de chaque activité.**

<img src="../../../img/suivi/achevement05.jpg" className='printscreen' style={{float:'right',maxWidth:'40%'}} />

Par exemple, pour un test, il est possible de paramétrer ces conditions :

 * <img src="../../../img/suivi/achevement14.jpg" style={{maxWidth:'20px'}} /> Choix 1 : "Les participants peuvent marquer manuellement cette activité comme terminée".

 * Choix 2 : "Afficher l'activité comme terminée dès que les conditions sont remplies". 
	
    Dans ce cas, Il y a plusieurs possibilités : 
    a. *Les étudiants doivent afficher l'activité pour la terminer*  
    b. *Les étudiants doivent recevoir une note pour terminer cette activité :*  
    ➡️ *Requiert la note de passage* : l'activité est considérée comme terminée lorsque l'étudiant reçoit une note suffisante (à définir dans la rubrique "Note" des paramètres de l'activité).  
    ➡️ *Ou toutes les tentatives terminées* : dans le cas où les élèves ont droit à un nombre précis de tentatives pour faire le test (à définir dans le paramètre Note > Nombre de tentatives autorisées) 


:::tip  **Exemple 1 : Le TEST sera terminé avec une note minimale**

 ➡️ Ouvrez les **Paramètres** de l'activité  

 <img src="../../../img/suivi/achevement06.jpg" className='printscreen' style={{float:'right',maxWidth:'30%'}} />

 ➡️ Dans la rubrique **"Notes"** :

 **1.**	Indiquez la **"note pour passer".**
 **2.**	Indiquez le **"nombre de tentatives autorisées".**
 
 <div style={{clear:'both'}}>&nbsp;</div>

 <img src="../../../img/suivi/achevement07.jpg" className='printscreen' style={{float:'right',maxWidth:'40%'}} />

 ➡️ Dans la rubrique **"Achèvement d'activité"** :

 **3.**	Sélectionnez **"Afficher l'activité comme terminée dès que les conditions sont remplies".**
 
 **4.**	Cochez les cases **"L'étudiant doit recevoir une note…"**, **"L'étudiant doit obtenir une note minimale de réussite"**
 et **"ou toutes les tentatives terminées".**
 
 **5.**	**Enregistrez**

<div style={{clear:'both'}}>&nbsp;</div>

   <img src="../../../img/suivi/achevement15.jpg" style={{maxWidth:'20px'}} /> (Coche bleue) N'importe quelle note permet d'achever l'activité.  

   <img src="../../../img/suivi/achevement16.jpg" style={{maxWidth:'20px'}} /> (Coche rouge) L'activité a été faite, mais sans atteindre la note minimale.  

   <img src="../../../img/suivi/achevement17.jpg" style={{maxWidth:'20px'}} /> (Coche verte) L'activité a été faite avec une note supérieure ou égale à la note minimale.
<div style={{clear:'both'}}>&nbsp;</div>         
:::


:::tip  **Exemple 2 : L'activité DEVOIR sera terminée quand l'élève aura déposé un travail** (sans condition de note).

 ➡️ Ouvrez les **Paramètres** de l'activité  

 <img src="../../../img/suivi/achevement08.jpg" className='printscreen' style={{float:'right',maxWidth:'40%'}} />

 ➡️ Dans la rubrique **"Achèvement d'activité"** :

**1.** Sélectionnez **"Afficher l'activité comme terminée dès que les conditions sont remplies".**

**2.** Cochez la case "L**e participant doit remettre quelque chose pour terminer cette activité".**

**3.** **Enregistrez**
<div style={{clear:'both'}}>&nbsp;</div>  
:::

## Le suivi d'achèvement côté Élève

 ➡️ Dans le cours, tous les granules paramétrés avec un suivi d'achèvement sont associés une information.

**Visible dès la page de cours** (voir l'encadré au début de cette fiche) :

 <img src="../../../img/suivi/achevement09.jpg" className='printscreen' style={{float:'right',maxWidth:'40%'}} />

 <div style={{clear:'both'}}>&nbsp;</div>

**Visible dans la page de l'activité** :

 <img src="../../../img/suivi/achevement10.jpg" className='printscreen' style={{float:'right',maxWidth:'40%'}} />

<div style={{clear:'both'}}>&nbsp;</div>


## Le suivi d'achèvement côté Enseignant

<img src="../../../img/suivi/achevement11.jpg" className='printscreen' style={{float:'right',maxWidth:'40%'}} />

**1.**	Dans votre cours, cliquez sur l'onglet **"Rapports".**

**2.**	Cliquez sur le menu **"Achèvement d'activités".**

<div style={{clear:'both'}}>&nbsp;</div>

<img src="../../../img/suivi/achevement12.jpg" className='printscreen' style={{float:'right',maxWidth:'40%'}} />

**3.**	Les **rapports d'achèvement d'activités** présentent…
 * La **liste des élèves** inscrits
 * L'achèvement de **chacune des activités paramétrées**

**Un export au format Tableur est possible.**

<div style={{clear:'both'}}>&nbsp;</div>

:::tip
<img src="../../../img/suivi/achevement13.jpg" className='printscreen' style={{float:'right',maxWidth:'20%'}} />

**Le bloc "Progression"** permet de rendre plus visuelle la progression dans le cours :

*Consultez la fiche Réflexe qui vous présente ce bloc.*

<div style={{clear:'both'}}>&nbsp;</div>
:::

<div style={{clear:'both'}}>&nbsp;</div>
