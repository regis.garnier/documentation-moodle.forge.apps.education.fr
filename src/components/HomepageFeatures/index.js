import React from 'react';
import clsx from 'clsx';
import styles from './styles.module.css';

const FeatureList = [
  {
    title: 'Tutoriels',
    link:'/docs/Tutoriels/intro',
    Svg: require('@site/static/img/undraw_file_searching_re_3evy.svg').default,
    description: (
      <>
        Vous cherchez à mettre en œuvre une activité, comment la paramétrer:
      </>
    ),
  },
  {
    title: 'Démonstration',
    link:'/docs/Demonstration/intro',
    Svg: require('@site/static/img/undraw_user_flow_re_bvfx.svg').default,
    description: (
      <>
        Vous cherchez des exemples d usages, avec leur objectifs.
      </>
    ),
  },
  {
    title: 'Scénarios',
    link:'/docs/Scenarisation/intro',
    Svg: require('@site/static/img/undraw_adventure_map_hnin.svg').default,
    description: (
      <>
        Vous cherchez comment créer des enchainements dans vos activités 
      </>
    ),
  },
];

function Feature({Svg, title, description, link}) {
  return (
    <div className={clsx('col col--4')}>
      <div className="text--center">
      <a href={link}><Svg className={styles.featureSvg} role="img" /></a>
      </div>
      <div className="text--center padding-horiz--md">
      <a href={link}><h3>{title}</h3></a>
        <p>{description}</p>
      </div>
    </div>
  );
}

export default function HomepageFeatures() {
  return (
    <section className={styles.features}>
      <div className="container">
        <div className="row">
          {FeatureList.map((props, idx) => (
            <Feature key={idx} {...props} />
          ))}
        </div>
      </div>
    </section>
  );
}
